{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Config.Types
    (
        module Config.Class
      , module Config.SectionTable
      , Config (..)
      , ConfigException (..)
      , Forest (..)
      , JSONConf
      , Map
      , ParsingError (..)
      , Parser
      , SectionedConf
      , SimpleConf
      , Text
      , Tree (..)
      , Value (..)
      , Yaml (..)
      , YamlConf
      , withTable
    ) where

import Control.Exception
import Control.Monad
import Data.Aeson
import Data.Aeson.Types (Parser)
import Data.Bifunctor (first)
import GHC.Exts (IsList (..))
import Data.List (intercalate)
import Data.Map (Map)
import qualified Data.Map as M
import Data.Text (Text, pack)
import Data.Text.Encoding (decodeUtf8, encodeUtf8)
import qualified Data.Yaml as Y
import Text.Megaparsec (takeRest)

import Config.Class hiding (findDuplicates, popElem)
import Config.Parser.Prim hiding (Parser)
import Config.SectionTable
import Data.NEList

type JSONConf       = Config (Forest Text) (NEList Text) Value
type SimpleConf     = Config (Map Text) Text JText
type SectionedConf  = Config (SectionTable Text) (Text,Text) JText
type YamlConf       = Config (Forest Text) (NEList Text) Yaml

data Config t k v
    = Config {
            getSchema   :: Maybe (t (ParamDescr v))
          , getStruct   :: FileStruct t k
          , getParams   :: t v
        }

instance (Empty t, Monoid (FileStruct t k)) => Empty (Config t k) where
    emptyT = Config Nothing mempty emptyT

instance (Monoid (FileStruct t k), ParamTable t k) => ParamTable (Config t k) k where
    alter f
        = withTable . alter f
    get k
        = get k . getParams
    list
        = list . getParams

withTable :: (t v -> t v) -> Config t k v -> Config t k v
withTable f cfg@Config{getParams = tbl}
    = cfg{getParams = f tbl}



data ConfigException
    = ValidationException {
            unsetPars   :: [String]
          , unknownPars :: [String]
        } deriving (Show)
instance Exception ConfigException where
    displayException (ValidationException unset unknown)
        = unlines 
                $ unlessNull unset "The following parameters has no value: "
                    <> unlessNull unknown "The following parameters are absent in schema: "
            where
                unlessNull xs msg
                    = if null xs
                      then
                        []
                      else
                        [msg <> intercalate ", " (show <$> xs)]



-- | a tree with arbitrary number of branches and no intermediate leafs
newtype Forest k v
    = Forest {
            getSubtrees :: Map k (Tree k v)
        } deriving (Eq, Show)

instance Functor (Forest k) where
    fmap f
        = Forest . fmap (fmap f) . getSubtrees

instance (Ord k) => IsList (Forest k v) where
  type Item (Forest k v) = (k,Tree k v)
  fromList  = Forest . fromList
  toList    = toList . getSubtrees

instance (Ord k) => Empty (Forest k) where
    emptyT = Forest emptyT

instance (FromJSON k, FromJSONKey k, Ord k, FromJSON v) => FromJSON (Forest k v) where
    parseJSON   = fmap Forest . parseJSON

instance (ToJSONKey k, ToJSON v) => ToJSON (Forest k v) where
    toJSON      = toJSON . getSubtrees
    toEncoding  = toEncoding . getSubtrees

instance (Ord k) => ParamTable (Forest k) (NEList k) where
    alter f (End x)
        = Forest . M.alter updVal x . getSubtrees
            where
                updVal (Just t)
                    = case t of
                        Leaf v  -> Leaf <$> f (Just v)
                        st      -> Just st
                updVal Nothing
                    = Leaf <$> f Nothing
    alter f pth@(x:|xs)
        = Forest . M.alter updVal x . getSubtrees
            where
                updVal (Just t)
                    = case t of
                        Subtrees st -> Just $ Subtrees $  alter f xs st
                        lf          -> Just lf
                updVal Nothing
                    = mkSubpath <$> f Nothing
                mkSubpath v
                    = foldr (\k -> Subtrees . Forest . M.singleton k) (Leaf v) pth
    get (End x) t
        = case M.lookup x $ getSubtrees t of
            Just (Leaf v)   -> Just v
            _               -> Nothing
    get (x:|xs) t
        = case M.lookup x $ getSubtrees t of
            Just (Subtrees t')  -> get xs t'
            _                   -> Nothing
    list
        = M.toList . getSubtrees
            >=> \(chk,val) -> case val of
                                Leaf v      -> return (End chk,v)
                                Subtrees t  -> first (chk:|) <$> list t

instance (FromJSON v, ToJSON v, TextValue v) => IsConfig (Forest Text) (NEList Text) v where
    type FileStruct (Forest Text) (NEList Text)
        = ()
    parseTable
        = takeRest >>= either fail (return . (,) ()) . decodeText
    serializeTable ()
        = decodeUtf8 . Y.encode
    _schemaParser v
        | Nothing <- v
            = _schemaParser $ Just curr_version
        | x == 0 && y == 1
            = parseJSON >=> mapM parseTree >=> return . Forest
        | otherwise
            = const $ fail $ "Unknown version: " <> show x <> "." <> show y
                where
                    ~(Just (Version x y))
                        = v
                    curr_version
                        = Version 0 1
                    -- parseTree :: Value -> Parser (Tree Text (ParamDescr v))
                    parseTree
                        = withObject "tree"
                            $ \o -> do
                                        isPar <- o .:! "param" .!= False
                                        if  isPar
                                        then
                                            Leaf <$> parseJSON (Object o)
                                        else
                                            o .: "parameters" >>= _schemaParser v >>= return . Subtrees



data Tree k v
    = Leaf v | Subtrees (Forest k v)
        deriving (Eq, Show)

instance Functor (Tree k) where
    fmap f (Leaf v)
        = Leaf $ f v
    fmap f (Subtrees xs)
        = Subtrees $ fmap f xs

instance (FromJSON k, FromJSONKey k, Ord k, FromJSON v) => FromJSON (Tree k v) where
    parseJSON o@(Object _)
        = Subtrees <$> parseJSON o
    parseJSON v
        = Leaf <$> parseJSON v
instance (ToJSONKey k, ToJSON v) => ToJSON (Tree k v) where
    toJSON (Leaf v)
        = toJSON v
    toJSON (Subtrees xs)
        = toJSON xs
    toEncoding (Leaf v)
        = toEncoding v
    toEncoding (Subtrees xs)
        = toEncoding xs



newtype Yaml
    = Yaml {
            getValue :: Value
        } deriving (Eq, Show)

instance FromJSON Yaml where
    parseJSON
        = fmap Yaml . parseJSON

instance ToJSON Yaml where
    toJSON
        = toJSON . getValue

instance TextValue Yaml where
    readValue
        = decodeText . pack
    valueToString
        = valueToString . getValue

decodeText :: (FromJSON a) => Text -> Either String a
decodeText
    = first Y.prettyPrintParseException . Y.decodeEither' . encodeUtf8
