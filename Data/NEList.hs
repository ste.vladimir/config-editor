{-# LANGUAGE TypeFamilies #-}

module Data.NEList
    (
        NEList (..)
      , filterNL
      , foldNL
      , fromList'
      , uncons
      , unconsNL
    ) where

import qualified Data.Foldable as F
import Data.List (intercalate)
import GHC.Exts (IsList (..))

data NEList a =
    End a | a :| NEList a deriving (Eq, Ord)
instance (Show a) => Show (NEList a) where
    show xs =
        "{" ++ (intercalate "," $ map show $ toList xs) ++ "}"
instance Functor NEList where
    fmap f (End x)  = End $ f x
    fmap f (x:|xs)  = f x :| fmap f xs
instance Applicative NEList where
    pure            = End
    (End f) <*> xs  = fmap f xs
    (f:|fs) <*> xs  = fmap f xs <> (fs <*> xs)
instance Semigroup (NEList a) where
    (End x) <> ys   = x :| ys
    (x:|xs) <> ys   = x :| (xs <> ys)
instance F.Foldable NEList where
    foldr f acc = foldNL (`f` acc) f
instance Traversable NEList where
    traverse f (End x)  = End <$> f x
    traverse f (x:|xs)  = (:|) <$> f x <*> traverse f xs

instance IsList (NEList a) where
    type Item (NEList a) = a

    fromList (x:xs) = fromList' x xs
    fromList []     = error "empty list"

    toList          = F.toList

fromList' :: a -> [a] -> NEList a
fromList' x (y:ys)  = x :| fromList' y ys
fromList' x []      = End x

foldNL :: (a -> b) -> (a -> b -> b) -> NEList a -> b
foldNL f _ (End x)  = f x
foldNL f g (x:|xs)  = g x (foldNL f g xs)

filterNL :: (a -> Bool) -> NEList a -> [a]
filterNL p = filter p . toList

uncons :: NEList a -> (a,[a])
uncons (End x)  = (x,[])
uncons (x:|xs)  = (x,toList xs)

unconsNL :: NEList a -> (a,Maybe (NEList a))
unconsNL (End x)    = (x,Nothing)
unconsNL (x:|xs)    = (x,Just xs)
