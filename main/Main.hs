{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE OverloadedStrings #-}

import Control.Monad
import Control.Monad.Catch
import Control.Monad.Trans
import Data.Bool (bool)
import Data.Proxy
import qualified Data.Text.IO as T
import Options.Applicative
import System.Exit (exitFailure)
import System.IO

import Config.Edit
import Config.Types hiding (Parser)
import Data.NEList

data Action (t :: * -> *) k v =
    Action {
        _inFile     :: Maybe FilePath
      , _outFile    :: Maybe FilePath
      , _confSchema :: Maybe FilePath
      , _whatToDo   :: Cmd k v
    }

data Cmd k v =
    Gen [(k,v)]
        | Get k Bool
        | List
        | Set [(k,v)]
        | Unset [k]

main :: IO ()
main =
    parseOpts `catches`
        [
            Handler $ \e@(ValidationException _ _) -> withExpt e
          , Handler $ \e@(ParsingError _) -> withExpt e
        ]

withExpt :: (Exception e) => e -> IO a
withExpt e
    = hPutStrLn stderr (displayException e) >> exitFailure

runAction :: (IsConfig t k v) => Proxy (Config t k v) -> Action t k v -> IO ()
runAction proxy (Action in_file out_file scm_file cmd) = do
    scm <- traverse (readSchemaFile proxy) scm_file
    let load        = loadConfig in_file
        checkValid  = maybe return validate scm
    case cmd of
        Gen xs ->
            checkValid (setParams xs emptyT)
                >>= saveConfig out_file
        Get par withoutNL ->
            load
                >>= checkValid
                >>= maybe
                        exitFailure
                        (liftIO . bool putStrLn putStr withoutNL . valueToString)
                        . get par
        List    ->
            load
                >>= checkValid
                >>= liftIO . T.putStr . serializeConfig . clearStruct
        Set xs  ->
            load
                >>= checkValid . setParams xs
                >>= saveConfig out_file
        Unset xs ->
            load
                >>= checkValid . unsetParams xs
                >>= saveConfig out_file

-- -- | reads from STDIN on 'Nothing'
loadConfig :: (IsConfig t k v, MonadIO m, MonadThrow m) => Maybe FilePath -> m (Config t k v)
loadConfig
    = maybe
        (liftIO T.getContents >>= parseConfig "<STDIN>")
        readConfigFile

setParams :: (ParamTable t k) => [(k,v)] -> t v -> t v
setParams xs cfg
    = foldl (\acc (k,v) -> insert k v acc) cfg xs

unsetParams :: (ParamTable t k) => [k] -> t v -> t v
unsetParams xs cfg
    = foldl (\acc k -> delete k acc) cfg xs

-- | writes to STDOUT on 'Nothing'
saveConfig :: (IsConfig t k v, MonadIO m) => Maybe FilePath -> Config t k v -> m ()
saveConfig out_file
    = liftIO . maybe T.putStr T.writeFile out_file . serializeConfig

parseOpts :: IO ()
parseOpts =
    join $ execParser $ flip info mempty $ helper <*> optsParser

optsParser :: Parser (IO ())
optsParser =
    hsubparser
        $ metavar "FORMAT"
        <> help "configuration file format"
        <> (simple <> section <> yaml <> json)

simple :: Mod CommandFields (IO ())
simple
    = mkAction
        "simple"
        "plain config represented as set of lines PARAM=VALUE"
        simpleToDo

section :: Mod CommandFields (IO ())
section
    = mkAction
        "section"
        "Windows-like INI file with [sections]"
        sectionToDo

yaml :: Mod CommandFields (IO ())
yaml
    = mkAction
        "yaml"
        "YAML config"
        yamlToDo

json :: Mod CommandFields (IO ())
json
    = mkAction
        "json"
        "JSON config"
        jsonToDo

simpleToDo :: Parser (Action (Map Text) Text JText)
simpleToDo = toDo

sectionToDo :: Parser (Action (SectionTable Text) (Text,Text) JText)
sectionToDo = toDo

yamlToDo :: Parser (Action (Forest Text) (NEList Text) Yaml)
yamlToDo = toDo

jsonToDo :: Parser (Action (Forest Text) (NEList Text) Value)
jsonToDo = toDo

mkAction :: (IsConfig t k v) => String -> String -> Parser (Action t k v) -> Mod CommandFields (IO ())
mkAction = mkCmdProxy Proxy

mkCmdProxy :: (IsConfig t k v) => Proxy (Config t k v) -> String -> String -> Parser (Action t k v) -> Mod CommandFields (IO ())
mkCmdProxy proxy name descr psr
    = mkCmd name descr $ runAction proxy <$> psr

toDo :: (TextKey k, TextValue v) => Parser (Action t k v)
toDo
    = hsubparser
        $ metavar "ACTION"
        <> help "what to do"
        <> (genCmd <> getCmd <> listCmd <> setCmd <> unsetCmd)



genCmd :: (TextKey k, TextValue v) => Mod CommandFields (Action t k v)
genCmd
    = mkCmd
        "generate"
        "generate configuration file from schema"
        $ Action
            <$> pure Nothing
            <*> optional outFileOpt
            <*> optional schemaOpt
            <*> (Gen <$> paramValues)

getCmd :: (TextKey k) => Mod CommandFields (Action t k v)
getCmd
    = mkCmd
        "get"
        "get value of the parameter"
        $ Action
            <$> optional inFileOpt
            <*> pure Nothing
            <*> optional schemaOpt
            <*> liftA2 Get paramNameArg suppressNewlineP

listCmd :: Mod CommandFields (Action t k v)
listCmd
    = mkCmd
        "list"
        "list all parameters and their values"
        $ Action
            <$> optional inFileOpt
            <*> pure Nothing
            <*> optional schemaOpt
            <*> pure List

setCmd :: (TextKey k, TextValue v) => Mod CommandFields (Action t k v)
setCmd
    = mkCmd
        "set"
        "set parameters"
        $ Action
            <$> optional inFileOpt
            <*> optional outFileOpt
            <*> optional schemaOpt
            <*> fmap Set paramValues
            <**> withInAsOut
            <**> toStdoutOpt

unsetCmd :: (TextKey k) => Mod CommandFields (Action t k v)
unsetCmd
    = mkCmd
        "unset"
        "unset the parameter"
        $ Action
            <$> optional inFileOpt
            <*> optional outFileOpt
            <*> optional schemaOpt
            <*> fmap Unset (many paramNameArg)
            <**> withInAsOut
            <**> toStdoutOpt

mkCmd :: String -> String -> Parser a -> Mod CommandFields a
mkCmd name descr p
    = command name $ p `info` progDesc descr 



schemaOpt :: Parser FilePath
schemaOpt
    = strOption
        $ long "with-schema"
        <> short 's'
        <> action "filenames"
        <> metavar "FILE"
        <> help "schema to validate file against"

suppressNewlineP :: Parser Bool
suppressNewlineP
    = switchMany
        $ short 'n'
        <> long "no-newline"
        <> help "do not print the trailing newline (useful in scripts)"

inFileOpt :: Parser FilePath
inFileOpt
    = strOption
        $ long "in"
        <> short 'f'
        <> action "filenames"
        <> metavar "FILE"
        <> help "configuration file to work with"

outFileOpt :: Parser FilePath
outFileOpt
    = strOption
        $ long "out"
        <> short 'o'        
        <> action "filenames"
        <> metavar "FILE"
        <> help "output file"

-- | if out-file is not specified defaults to in-file
withInAsOut :: Parser (Action t k v -> Action t k v)
withInAsOut
    = pure $ \act@Action{_outFile = out_file} -> act{_outFile = out_file <|> _inFile act}

toStdoutOpt :: Parser (Action t k v -> Action t k v)
toStdoutOpt
    = flagMany id (\act -> act{_outFile = Nothing})
        $ long "stdout"
        <> help "print to <STDOUT> instead of modifying input file"

paramNameArg :: (TextKey k) => Parser k
paramNameArg
    = argument (eitherReader readKey) $ metavar "PARAM"

paramValues :: (TextKey k, TextValue v) => Parser [(k,v)]
paramValues
    = many $ argument (eitherReader readPar) $ metavar "PARAM=VALUE ..."
        where
            readPar s
                = let (par,val) = break (== '=') s
                  in
                    liftM2
                        (,)
                        (readKey par)
                        $ readValue $ bool (tail val) val (null val)

flagMany :: a -> a -> Mod FlagFields () -> Parser a
flagMany on_null on_xs
    = (bool on_xs on_null . null <$>)
        . many
        . flag' ()

-- | like @switch@, but does not fail if flag is supplied more than once
switchMany :: Mod FlagFields () -> Parser Bool
switchMany
    = flagMany False True
