{-# LANGUAGE OverloadedStrings #-}

module ExecutableTest (exeTest) where

import System.Exit
import Test.Tasty

import Utility

exeTest :: TestTree
exeTest
    = testGroup "Executable"
        [
            testGroup "simple"
                $ map ($ "simple")
                    [
                        genCmd ["-s", "simple.conf.yaml", "baz=2", "quiz=4"]
                            $ resOk ["bar=1", "baz=2", "foo=3", "quiz=4"]
                      , getCmd ["-f", "simple.conf", "foo"]
                            $ resOk ["3"]
                      , getCmd ["-f", "simple.conf", "foobar"]
                            $ (ExitFailure 1, "", "")
                      , listCmd ["-f", "simple.conf"]
                            $ resOk ["bar=1", "baz=2", "foo=3", "quiz=4"]
                      , setCmd ["-f", "simple.conf", "x=5"]
                            $ resOk [
                                        ""
                                      , "bar=1"
                                      , "#Some comments"
                                      , ""
                                      , "baz=2"
                                      , ""
                                      , "## more"
                                      , "# comment"
                                      , ""
                                      , "foo=3"
                                      , "quiz=4"
                                      , "x=5"
                                    ]
                      , unsetCmd ["-f", "simple.conf", "bar"]
                            $ resOk [
                                        ""
                                      , "#Some comments"
                                      , ""
                                      , "baz=2"
                                      , ""
                                      , "## more"
                                      , "# comment"
                                      , ""
                                      , "foo=3"
                                      , "quiz=4"
                                    ]
                    ]
          , testGroup "section"
                $ map ($ "section")
                    [
                        genCmd ["-s", "sectioned.conf.yaml", "sect1/bar=1", "sect2/quiz=4", "sect2/baz=2"]
                            $ resOk ["[sect1]", "bar=1", "foo=3", "[sect2]", "baz=2", "quiz=4"]
                      , getCmd ["-f", "sectioned.conf", "sect1/foo"]
                            $ resOk ["3"]
                      , getCmd ["-f", "sectioned.conf", "sect1/foobar"]
                            $ (ExitFailure 1, "", "")
                      , listCmd ["-f", "sectioned.conf"]
                            $ resOk ["[sect1]", "bar=2", "foo=3", "[sect2]", "baz=1", "[sect3]"]
                      , setCmd ["-f", "sectioned.conf", "sect3/bar=1", "sect2/baz=5"]
                            $ resOk [
                                        "# header"
                                      , "[sect3]"
                                      , ""
                                      , "### comment ###"
                                      , ""
                                      , "bar=1"
                                      , "[sect1]"
                                      , "foo=3"
                                      , "# the comment"
                                      , "bar=2"
                                      , ""
                                      , ""
                                      , "[sect2]"
                                      , ""
                                      , "baz=5"
                                    ]
                      , unsetCmd ["-f", "sectioned.conf", "sect3/bar", "sect2/baz"]
                            $ resOk [
                                        "# header"
                                      , "[sect3]"
                                      , ""
                                      , "### comment ###"
                                      , ""
                                      , "[sect1]"
                                      , "foo=3"
                                      , "# the comment"
                                      , "bar=2"
                                      , ""
                                      , ""
                                      , "[sect2]"
                                      , ""
                                    ]
                    ]
        ]
