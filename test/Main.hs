import System.Directory (setCurrentDirectory)
import Test.Tasty

import ConfigParserTest
import ExecutableTest
import SchemaParserTest
import SerializationTest

main :: IO ()
main = do
    setCurrentDirectory "test/examples"
    defaultMain $ testGroup "Basic Tests"
            [
                configParserTest
              , schemaParserTest
              , serializeConfigTest
              , exeTest
            ]
