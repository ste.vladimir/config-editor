{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}

module SchemaParserTest (schemaParserTest) where

import Data.Proxy
import Test.Tasty

import Config.Edit
import Config.Types
import Utility

schemaParserTest :: TestTree
schemaParserTest =
    testGroup "Schema Parsing"
        [
            runTest "simple.conf.yaml"
                (Proxy :: Proxy SimpleConf)
                [
                    ("bar", Just "1")
                  , ("baz", Nothing)
                  , ("foo", Just "3")
                  , ("quiz", Nothing)
                ]
          , runTest "simple_empty.conf.yaml"
                (Proxy :: Proxy SimpleConf)
                []
          , runTest "sectioned.conf.yaml"
                (Proxy :: Proxy SectionedConf)
                [
                    ("sect1", [("foo", Just "3"), ("bar", Nothing)])
                  , ("sect2", [("baz", Just "1"), ("quiz", Nothing)])
                ]
          , runTest "sectioned_empty.conf.yaml"
                (Proxy :: Proxy SectionedConf)
                []
          , runTest "yaml.conf.yaml"
                (Proxy :: Proxy YamlConf)
                [
                    ("foo", Leaf $ Just $ Yaml $ Number 3)
                  , ("bar", Leaf $ Just $ Yaml $ Number 1)
                  , ("baz", Subtrees [("quiz", Leaf Nothing)])
               ]
        ]
            where
                runTest file p  = doTests file (readSchema p file) . return
                readSchema p    = fmap (fmap defaultValue . paramsDescr) . readSchemaFile p
