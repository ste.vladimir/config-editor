{-# LANGUAGE FlexibleContexts #-}

module SerializationTest (serializeConfigTest) where

import Data.Proxy
import Data.Text.IO
import Test.Tasty

import Config.Edit
import Config.Types
import Utility

serializeConfigTest :: TestTree
serializeConfigTest =
    testGroup "parse=>serialize"
        [
            readAndSave (Proxy :: Proxy SimpleConf) "simple.conf"
                `simpleTest`
            Data.Text.IO.readFile "simple.conf.normalized"
          , readAndSave (Proxy :: Proxy SectionedConf) "sectioned.conf"
                `sectionTest`
            Data.Text.IO.readFile "sectioned.conf.normalized"
        ]
            where
                readAndSave p
                    = fmap serializeConfig . readConfig p
