{-# LANGUAGE FlexibleContexts #-}

module Utility where

import System.Exit
import System.Process
import Test.Tasty
import Test.Tasty.HUnit

import Config.Edit
import Config.Types
import Data.Proxy

doTests :: (Eq v, Show v)
        => String
        -> IO v
        -> IO v
        -> TestTree
doTests name act get_expect
    = testCase name $ do
        res <- act
        tst <- get_expect
        res @?= tst

readConfig   :: (IsConfig t k v) => Proxy (Config t k v) -> FilePath -> IO (Config t k v)
readConfig _ = readConfigFile

simpleTest,sectionTest,yamlTest :: (Eq v, Show v) => IO v -> IO v -> TestTree
simpleTest  = doTests "simple"
sectionTest = doTests "section"
yamlTest    = doTests "yaml"

genCmd :: [String] -> (ExitCode,String,String) -> String -> TestTree
genCmd args expect t
    = testCommand t "generate" args expect

getCmd :: [String] -> (ExitCode,String,String) -> String -> TestTree
getCmd args expect t
    = testCommand t "get" args expect

listCmd :: [String] -> (ExitCode,String,String) -> String -> TestTree
listCmd args expect t
    = testCommand t "list" args expect

setCmd :: [String] -> (ExitCode,String,String) -> String -> TestTree
setCmd args expect t
    = testCommand t "set" ("--stdout":args) expect

unsetCmd :: [String] -> (ExitCode,String,String) -> String -> TestTree
unsetCmd args expect t
    = testCommand t "unset" ("--stdout":args) expect

testCommand :: String -> String -> [String] -> (ExitCode,String,String) -> TestTree
testCommand conf_type action args expect
    = testCase action
        $ do
            res <- readCreateProcessWithExitCode (execCmd conf_type action args) ""
            res @?= expect

execCmd :: String -> String -> [String] -> CreateProcess
execCmd conf_type action xs
    = proc "stack"
        $ ["exec", "--", "config-editor", conf_type, action] <> xs

resOk :: [String] -> (ExitCode,String,String)
resOk out = (ExitSuccess,unlines out,"")
